package org.alexd.kio

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlin.coroutines.CoroutineContext

sealed class KIO<out A> {
    fun <B> map(f: (A) -> B): KIO<B> =
        flatMap { a -> just(f(a)) }

    fun <B> flatMap(f: (A) -> KIO<B>): KIO<B> =
        FlatMap(this, f)

    fun attempt(): KIO<Either<Throwable, A>> =
        Attempt(this)

    fun recover(f: (Throwable) -> KIO<@UnsafeVariance A>): KIO<A> =
        attempt().flatMap {
            when (it) {
                is Left -> f(it.left)
                is Right -> just(it.right)
            }
        }

    fun <B> bracket(release: (A) -> KIO<Unit>, use: (A) -> KIO<B>): KIO<B> =
        Bracket(this, use, release)

    fun runOn(context: CoroutineContext): KIO<A> =
        RunOn(this, context)

    fun <B, C> parMap(other: KIO<B>, f: (A, B) -> C): KIO<C> =
        ParMap(this, other, f)

    companion object {
        operator fun <A> invoke(f: suspend IOScope.() -> A): KIO<A> =
            Suspend(f)

        fun <A> just(value: A): KIO<A> =
            Return(value)

        fun failed(error: Throwable): KIO<Nothing> =
            Error(error)
    }

    suspend fun unsafeRun() = unsafeRun(this, IOScope())
}

private sealed class DirectIO<A> : KIO<A>() {
    abstract suspend fun runScoped(scope: IOScope): A
}

private data class Return<A>(val value: A) : DirectIO<A>() {
    override suspend fun runScoped(scope: IOScope) = value
}

private data class Error(val error: Throwable) : DirectIO<Nothing>() {
    override suspend fun runScoped(scope: IOScope) = throw error
}

private data class Suspend<A>(val resume: suspend IOScope.() -> A) : DirectIO<A>() {
    override suspend fun runScoped(scope: IOScope) = with(scope) { resume() }
}

sealed class Either<out L, out R>
data class Left<L>(val left: L) : Either<L, Nothing>()
data class Right<R>(val right: R) : Either<Nothing, R>()

private data class Attempt<A>(val io: KIO<A>) : DirectIO<Either<Throwable, A>>() {
    override suspend fun runScoped(scope: IOScope): Either<Throwable, A> {
        return try {
            Right(unsafeRun(io, scope))
        } catch (c: CancellationException) {
            throw c
        } catch (t: Throwable) {
            //TODO handle fatal
            Left(t)
        }
    }
}

private data class Bracket<A, B>(
    val io: KIO<A>,
    val use: (A) -> KIO<B>,
    val release: (A) -> KIO<Unit>
) : DirectIO<B>() {
    override suspend fun runScoped(scope: IOScope): B {
        val resource = withContext(NonCancellable) { unsafeRun(io, scope) }
        return try {
            unsafeRun(use(resource), scope)
        } finally {
            withContext(NonCancellable) { unsafeRun(release(resource), scope) }
        }
    }
}

private data class RunOn<A>(
    val io: KIO<A>,
    val context: CoroutineContext
) : DirectIO<A>() {
    override suspend fun runScoped(scope: IOScope): A = withContext(context) {
        unsafeRun(io, scope)
    }
}

private data class ParMap<A, B, C>(
    val ioA: KIO<A>,
    val ioB: KIO<B>,
    val f: (A, B) -> C
) : DirectIO<C>() {
    override suspend fun runScoped(scope: IOScope): C = coroutineScope {
        val a = async { unsafeRun(ioA, scope) }
        val b = async { unsafeRun(ioB, scope) }
        f(a.await(), b.await())
    }
}

private data class ParTraverse<A, B>(
    val l: Iterable<A>,
    val f: (A) -> KIO<B>
) : DirectIO<List<B>>() {
    override suspend fun runScoped(scope: IOScope): List<B> = coroutineScope {
        val tasks = l.map { a -> async { unsafeRun(f(a), scope) } }
        tasks.map { it.await() }
    }
}

private data class ParTraverseN<A, B>(
    val l: Iterable<A>,
    val concurrency: Int,
    val f: (A) -> KIO<B>
) : DirectIO<List<B>>() {
    override suspend fun runScoped(scope: IOScope): List<B> = coroutineScope {
        val semaphore = Channel<Unit>(concurrency)
        val tasks = l.map { a ->
            async {
                semaphore.send(Unit)
                unsafeRun(f(a), scope).also { semaphore.receive() }
            }
        }
        tasks.map { it.await() }
    }
}

private data class FlatMap<A, B>(val sub: KIO<A>, val f: (A) -> KIO<B>) : KIO<B>()

@Suppress("UNCHECKED_CAST")
private tailrec suspend fun <A> unsafeRun(io: KIO<A>, scope: IOScope): A = when (io) {
    is DirectIO -> io.runScoped(scope)
    is FlatMap<*, A> -> {
        val f = io.f as (Any?) -> KIO<A>
        when (val sub = io.sub) {
            is DirectIO -> unsafeRun(f(sub.runScoped(scope)), scope)
            is FlatMap<*, *> -> {
                val g = sub.f as (Any?) -> KIO<A>
                val y = sub.sub
                unsafeRun(y.flatMap { a -> g(a).flatMap(f) }, scope)
            }
        }
    }
}

class IOScope {
    suspend fun <A> KIO<A>.run() = unsafeRun(this, this@IOScope)
}

fun <A, B> Iterable<A>.parTraverse(f: (A) -> KIO<B>): KIO<List<B>> =
    ParTraverse(this, f)

fun <A, B> Iterable<A>.parTraverseN(concurrency: Int, f: (A) -> KIO<B>): KIO<List<B>> =
    ParTraverseN(this, concurrency, f)

suspend fun runMain(f: () -> KIO<Unit>) = f().unsafeRun()