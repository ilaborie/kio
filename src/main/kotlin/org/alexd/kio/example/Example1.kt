package org.alexd.kio.example

import kotlinx.coroutines.*
import org.alexd.kio.KIO
import java.util.concurrent.atomic.AtomicInteger
import kotlin.random.Random

fun main() {
  runBlocking {
    val program = KIO {
      val n = AtomicInteger(0)
      val r = AtomicInteger(0)

      val acquireFile = KIO {
        val f = n.getAndIncrement()
        println("Acquiring File $f ${Thread.currentThread().name}")
        f
      }.runOn(Dispatchers.IO)

      fun releaseFile(f: Int) = KIO {
        println("Releasing file $f ${Thread.currentThread().name}")
        r.getAndIncrement()
        Unit
      }.runOn(Dispatchers.IO)

      val work = acquireFile.bracket(::releaseFile) {
        KIO {
          println("Working ${Thread.currentThread().name}")
          //delay(10)
          if (it < 99000) {
            println("Boom")
            throw Exception()
          } else {
            println("Done")
            42
          }
        }.runOn(Dispatchers.Unconfined)
      }

      fun retryMany(tries: Int): KIO<Int> = work.recover {
        if (tries > 0) retryMany(tries - 1) else KIO.failed(it)
      }

      val job = retryMany(100000).runOn(Dispatchers.Default)
      val twoJob = job.parMap(job) { a, b -> a + b }
      //println(unsafeRun(twoJob, this))

      val d = GlobalScope.async { twoJob.unsafeRun() }

      delay(Random.nextLong(1000))
      d.cancelAndJoin()

      if (n.get() != r.get())
        error("Illegal state ${n.get()} - ${r.get()}")

      n to r
    }

    fun putStrLn(s: String) = KIO { println(s) }

    fun loop(): KIO<Unit> = program.flatMap { (n, r) -> KIO {
      putStrLn("Successful run $n $r").run()
      delay(1000)
    } }.flatMap { loop() }

    val main = KIO {
      putStrLn("Hello").run()
      loop().run()
    }
    main.unsafeRun()
  }
}