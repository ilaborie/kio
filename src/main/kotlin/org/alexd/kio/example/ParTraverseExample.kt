package org.alexd.kio.example

import kotlinx.coroutines.*
import org.alexd.kio.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.random.Random

suspend fun main() = runMain {
  val n = AtomicInteger(0)
  val r = AtomicInteger(0)

  val acquireFile = KIO {
    val f = n.getAndIncrement()
    println("Acquiring File $f ${Thread.currentThread().name}")
    f
  }.runOn(Dispatchers.IO)

  fun releaseFile(f: Int) = KIO {
    println("Releasing file $f ${Thread.currentThread().name}")
    r.getAndIncrement()
    Unit
  }.runOn(Dispatchers.IO)

  fun work(task: String) = acquireFile.bracket(::releaseFile) {
    KIO {
      println("Starting $task in ${Thread.currentThread()} with file $it")
      if (task == "Task 43") {
        delay(200)
        error("Boom")
      }
      delay(1000)
      println("Done work $task")
      task
    }.runOn(Dispatchers.Unconfined)
  }.attempt()

  val list = (1 .. 1000).map { "Task $it" }

  fun putStrLn(s: String) = KIO { println(s) }

  val program = list.parTraverseN(100, ::work).flatMap { KIO {
    println("Result $it")
  } }


  KIO {
    //val d = GlobalScope.async { unsafeRun(program) }
    //delay(500)
    //d.cancelAndJoin()

    program.run()

    if (n.get() != r.get())
      error("Illegal state ${n.get()} - ${r.get()}")
    else
      println("Done $n $r")
  }
}