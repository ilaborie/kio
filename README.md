# KIO #

## Introduction ##

KIO is a lightweight effect monad (aka IO monad) for Kotlin, that is intended to
work naturally with Kotlin coroutines.

KIO allows a functional programming style where effectful computations such as
I/O are composed together then run in the highest layer of your application.

This approach has, IMHO, the following benefits :

  * Side-effecting function (local filesystem, http calls, etc ...) and pure
    function (business logic, algorithm, pure logic) are easily identified by
    their return type.
  * Working with small IO tasks, composing them into bigger tasks using
    functions and passing immutable data, allow to think in data-flow, without
    context and global state, which in turn leads to better and more robust
    software as well as better code reuse.
  
Effects & monads is a very broad subject, and their usage and benefits are
highly discussed. You can find a list of interesting resource on the subject in
the Reference section below.

Pragmatically, you can see KIO as functional combinator library for couroutines:
you have operators for running them in sequential, parallel, race, handling
error and retries, etc ...

## Design  ##

The design idea is to be a thin functional layer on top of the wonderful Kotlin
runtime which have a strong foundation for building an effect monad :

  * Supports running many coroutines
  * Good support for theading, dispatcher, scheduling
  * Built-in cancellation mechanism with `coroutineScope`
  
Since coroutines allow to write async code in a sequential fashion, classical
techniques for building synchronous monad such as trampoline monad, can be made
asynchronous without special treatment, which leads to very simple, yet
powerful, design.

## Examples ##

COMING SOON 😀 please see the unit test

### Wrapping a pure value ###

### Suspending a couroutine/side-effecting code inside a IO ###

### Composing effects sequentially ###

### Composing two effects in parallel ###

### Catching errors/retrying ###

### Handling resource in a deterministic fashion ###

### Changing execution context ###

### Running a list of effects in parallel ###

## References ##

TODO
